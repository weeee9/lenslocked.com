package models

import "strings"

var (
	// ErrNotFound is returned when a resource can not be
	// found is database.
	ErrNotFound modelError = "models: Resource not found"

	// ErrPasswordIncorrect is returende when an invalid password
	// is used when attempting to authenticate a user
	ErrPasswordIncorrect modelError = "models: Incorrect password provided"

	// ErrEmailRequired is returned when an email address is
	// not provided when creating a user
	ErrEmailRequired modelError = "models: Email address is required"

	// ErrEmailInvalid is returned when an eamil address provided
	// does not match any of our requirements
	ErrEmailInvalid modelError = "models: Email address is not valid"

	// ErrEmailTaken is returned when an update or create is attempted
	// with an eamil address thea is already in use.
	ErrEmailTaken modelError = "models: Email address is already taken"

	// ErrPasswordTooShort is returned when an update or create is
	// attempted with a user password that is less than 8 characters
	ErrPasswordTooShort modelError = "models: Password must be at least 8 characters long"

	// ErrPasswordRequired is returned when a create is attempted
	// without a user password provided.
	ErrPasswordRequired modelError = "models: Password is required"

	// ErrTitleRequired is returend when an
	ErrTitleRequired modelError = "models: Title is required"

	// ErrIDInvalid is retuned when an invalid ID is porvided
	// to a method like Delete()
	ErrIDInvalid privateError = "models: ID provided was invalid"

	// ErrRememberTooShort is returned when a remember token is not
	// at tleast 32 bytes.
	ErrRememberTooShort privateError = "models: Remember token must at least 32 bytes"

	// ErrRememberRequired is returned when an update or create is
	// attempted with a user remember token hash
	ErrRememberRequired privateError = "models: Remember token is required"

	// ErrUserIDRequired is returend when an
	ErrUserIDRequired privateError = "models: UserID is required"
)

type modelError string

func (e modelError) Error() string {
	return string(e)
}

func (e modelError) Public() string {
	s := strings.Replace(string(e), "models: ", "", 1)
	return s
}

type privateError string

func (e privateError) Error() string {
	return string(e)
}
