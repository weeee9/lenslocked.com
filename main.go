package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"lenslocked.com/controllers"
	"lenslocked.com/middleware"
	"lenslocked.com/models"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "weeee9"
	password = "your-pasword"
	dbname   = "lenslocked_dev"
)

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	services, err := models.NewServices(psqlInfo)
	if err != nil {
		panic(err)
	}
	defer services.Close()
	// services.DestructiveReset()
	services.AutoMigrate()

	router := mux.NewRouter()

	staticC := controllers.NewStatic()
	usersC := controllers.NewUsers(services.User)
	galleryC := controllers.NewGalleries(services.Gallery, services.Image, router)

	userMw := middleware.User{
		UserService: services.User,
	}
	requireUserMw := middleware.RequireUser{
		User: userMw,
	}

	// User routes
	router.Handle("/", staticC.HomeView).Methods("GET")
	router.Handle("/contact", staticC.ContactView).Methods("GET")
	router.HandleFunc("/signup", usersC.New).Methods("GET")
	router.HandleFunc("/signup", usersC.Create).Methods("POST")
	router.Handle("/login", usersC.LoginView).Methods("GET")
	router.HandleFunc("/login", usersC.Login).Methods("POST")

	// Imgae routes
	imageHandler := http.FileServer(http.Dir("./images/"))
	router.PathPrefix("/images/").Handler(http.StripPrefix("/images/", imageHandler))

	// Gallery routes
	router.Handle("/galleries", requireUserMw.ApplyFn(galleryC.Index)).Methods("GET")
	router.Handle("/galleries/new", requireUserMw.Apply(galleryC.New)).Methods("GET")
	router.HandleFunc("/galleries", requireUserMw.ApplyFn(galleryC.Create)).Methods("POST")

	router.HandleFunc("/galleries/{id:[0-9]+}/edit", requireUserMw.ApplyFn(galleryC.Edit)).Methods("GET").Name(controllers.EditGallery)
	router.HandleFunc("/galleries/{id:[0-9]+}/update", requireUserMw.ApplyFn(galleryC.Update)).Methods("POST")
	router.HandleFunc("/galleries/{id:[0-9]+}/delete", requireUserMw.ApplyFn(galleryC.Delete)).Methods("POST")
	router.HandleFunc("/galleries/{id:[0-9]+}", galleryC.Show).Methods("GET").Name(controllers.ShowGallery)

	router.HandleFunc("/galleries/{id:[0-9]+}/images", requireUserMw.ApplyFn(galleryC.ImageUpload)).Methods("POST")
	// /galleries/:id/images/:filename/delete
	router.HandleFunc("/galleries/{id:[0-9]+}/images/{filename}/delete", requireUserMw.ApplyFn(galleryC.ImageDelete)).Methods("POST")

	fmt.Println("Starting the server on :3000...")
	http.ListenAndServe(":3000", userMw.Apply(router))
}
